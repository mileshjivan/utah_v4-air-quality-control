/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author 
 */
public class Air_Quality_Monitoring_PageObjects extends BaseClass {

    public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

     public static String iframeName()
    {
        return "ifrMain";
    }
     
    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }    
   
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }

    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }    
    
    public static String saveWaitNew(){
        return ".//div[text()='Saving...']";
    }

   public static String tailingManagementTab(){
       return "(//label[contains(text(),'Tailings Management')])[1]";
   }
   public static String tailingManagementMonitoringTab(){
       return "//label[contains(text(),'Tailings Management Monitoring')]";
   }
   public static String airQualityMonitoringTab(){
       return "//label[contains(text(),'Dust Fallout Monitoring')]";
   }
   public static String addButton() {
        return "//div[@id='btnActAddNew']"; 
    }
   public static String businessUnitTab(){
       return "//div[@id='control_739C430A-AAD9-47C4-80E3-30FE13AB4D33']";
   }
   public static String businessUnitSelect(String text) {
        return "//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]";
   }
   public static String airQualityType(){
       return "//div[@id='control_164E9ECF-98B7-44BE-810E-9E6D002BE0EB']";
   }
   public static String airQualityTypeSelect(String text){
       return "//div[@id='divForms']/div[6]//a[contains(text(),'" + text + "')]";
   }
   public static String monthTab(){
       return "//div[@id='control_4E1D4CE9-349A-491A-8D18-EFD53A2076C7']";
   }
   public static String monthOption(String text){
       return "//div[@id='divForms']/div[7]//a[contains(text(),'" + text + "')]";
   }
   public static String yearTab(){
       return "//div[@id='control_228918B1-E384-4DE9-9910-2D881B26DA23']";
   }        
    public static String yearOption(String text){
       return "//div[@id='divForms']/div[8]//a[contains(text(),'" + text + "')]";
   }
    public static String monitoringPointTab(){
        return "//div[@id='control_E96E3D7B-B465-4EEE-B615-F90459343BE6']";
    }
    public static String monitoringPointOption(String text){
        return "(//div[@id='divForms']//a[contains(text(),'" + text + "')])[1]";
    }
    public static String uploadLinkbox() {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }
    public static String LinkURL(){
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle(){
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }
    public static String urlAddButton(){
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }
    public static String saveToContinueButton(){
        return "//div[@id='control_D4D50C09-26C5-410D-9549-2CC4BE86A134']";
    }
    
    public static String saveButton(){
        return "//div[@id='btnSave_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }
    
    
    public static String saveSupportingDocuments(){
        return "//div[@id='control_F1395E4D-7A99-4032-A127-20F4D7E31DE2']";
    }

    public static String airQualityMonitorRecordNumber_xpath() {
        return "(//div[@id='form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String emissionMeasureRecordNumber_xpath() {
        return "(//div[@id='form_BCE8C74D-00E6-4B6A-AAEF-C92A9F7FDC22']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String dustMeasureRecordNumber_xpath() {
        return "(//div[@id='form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String findingsRecordNumber_xpath() {
        return "(//div[@id='form_A4BB87E5-A0CA-4A12-A57D-06244726EDAA']//div[contains(text(),'- Record #')])[1]";
    }
    
    
    public static String AQMFindingsPanel(){
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']//span[text()='Air Quality Monitoring Findings']";
    }

    public static String parameterReadingsPanel(){
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']//span[text()='Parameter Readings']";
    }
       
    public static String measurementsPanel(){
        return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']//span[text()='Measurements']";
    }
        
    
    public static String processFlow2() {
        return "//div[@id='btnProcessFlow_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    //measurements Xpaths
    public static String measurementTab() {
         return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']";
    }
    public static String measurement(){
        return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']//span[contains(text(),'Measurements')]";
    }

    public static String measurementsAddButton() {
        return "//div[@id='control_4117C574-64DE-4D2D-810F-0C8EF1E4E1F1']//div[contains(text(),'Add')]";
    }
    public static String measurementProcessFlow(){
        return "//div[@id='btnProcessFlow_form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']";
    }
    public static String measurementMonitoringTab(){
        return "//div[@id='control_A6D81C10-F563-40CE-83BF-19B9B5F432F4']//li";
    }

    public static String measurementMonitoringOption(String text) {
        return "(//a[text()='"+text+"']/../i)[1]";
    }

    public static String measurementMonitoringOption2(String data) {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    
    
    public static String measurementFieldTab() {
        return "//div[@id='control_0F289C2C-8DDB-4551-A052-E90F299940E2']";
    }
    public static String measurementField(){
        return "(//div[@id='control_0F289C2C-8DDB-4551-A052-E90F299940E2']//input)[1]";
    }

    public static String unitTab() {
        return "//div[@id='control_E743CAC3-3341-4CB7-9EF3-4863E15E63DA']";
    }

    public static String unitOption(String data) {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String measurementSaveButton() {
        return "//div[@id='btnSave_form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']/div[contains(text(),'Save')]";
    }

    public static String CEM_Parameter_panel(){
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']//span[text()='Parameter Readings']";
    }
    
    public static String CEM_Add(){
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']//div[text()='Add']";
    }
    
    public static String CEM_Parameters_dropdown(){
        return "//div[@id='control_F11D69C1-F470-4A98-AA57-C7DA50E56DA4']//ul";
    }
    
    public static String CEM_Parameters_select(String data){
        return "(//div[@id='divForms']//a[contains(text(),'" + data + "')])[1]";
    }
    
    public static String CEM_Measurement(){
        return "//div[@id='control_BC956D1B-8B55-469B-8956-895C33B5E897']/div/div/input";
    }
    
    public static String CEM_Sdate(){
        return "//div[@id='control_661EDBD4-2302-4210-A42C-FBF1F7E2F674']//input";
    }
    
    public static String CEM_stakenby(){
        return "//div[@id='control_293D62D6-7F8B-4F4C-935C-8D9E57CB23B0']/div/div//input";
    }
    
    public static String CEM_comments(){
        return "//div[@id='control_E9887CF7-47E3-45E3-88CB-B23729525ECC']/div/div//textarea";
    }
    
    public static String CEM_Save(){
        return "//div[@id='btnSave_form_BCE8C74D-00E6-4B6A-AAEF-C92A9F7FDC22']";
    }
    
    public static String AQMF_panel(){
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']//span[text()='Air Quality Monitoring Findings']";
    }
    
    public static String AQMF_Add(){
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']//div[text()='Add']";
    }
    
    public static String AQMF_processflow(){
        return "//div[@id='btnProcessFlow_form_A4BB87E5-A0CA-4A12-A57D-06244726EDAA']";
    }
    
    public static String AQMF_desc(){
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }
    
    public static String AQMF_owner_dropdown(){
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }
    
    public static String AQMF_owner_select(String text){
        return "//a[contains(text(),'"+text+"')]";
    }
    
    public static String AQMF_risksource_dropdown(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }
    
    public static String AQMF_risksource_select(String text){
        return "//a[text()='"+text+"']/../i";
    }
    
    public static String AQMF_risksource_select2(String text1, String text2){
        return "//a[text()='"+text1+"']/..//a[text()='"+text2+"']/i[1]";
    }
    
    public static String AQMF_class_dropdown(){
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//li";
    }
    
    public static String AQMF_class_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String AQMF_risk_dropdown(){
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']";
    }
    
    public static String AQMF_risk_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String AQMF_arrow(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']/div/a/span[2]/b[1]";
    }
    
    public static String AQMF_save(){
        return "//div[@id='btnSave_form_A4BB87E5-A0CA-4A12-A57D-06244726EDAA']";
    }
    
    public static String AQMF_savewait(){
        return "//div[text()='Air Quality Findings Actions']";
    }
    public static String expandGlobalCompany() {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'Global Company')]//..//i)[1]";
    }
    public static String expandSouthAfrica(){
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'South Africa')]//..//i)[1]";
    }
    public static String CEM_processFlow() {
        return "//div[@id='btnProcessFlow_form_BCE8C74D-00E6-4B6A-AAEF-C92A9F7FDC22']";
    }

    public static String CEM_uploadLinkbox() {
        return "//div[@id='control_F60F4398-06EE-403F-891A-BFEA6314167E']//b[@class='linkbox-link']";
    }
}
