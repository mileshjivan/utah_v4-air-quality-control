/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author fnell
 */
public class SugarCRM_PageObject extends BaseClass
{
    public static int var_No;
    public static String varcellPhone;
    public static String var_Date;
        
    public static String SugarURL(){
        return "http://gts-training.dvt.co.za:81/sugarcrm/index.php";
    }
    
    //Generate Random Number
    public static int randomNumber(){
        Random rand = new Random();

        int  number = rand.nextInt(50) + 1;
        return number;
    }
    public static String randomCellPhone(){
        int num1, num2, num3; 
        int set2, set3; 
        String cellphone;
        
        Random generator = new Random();
        
        num1 = generator.nextInt(7);
        num2 = generator.nextInt(8)+1; 
        num3 = generator.nextInt(8);
        
        set2 = generator.nextInt(643) + 100;
        set3 = generator.nextInt(8999) + 1000;
        
        cellphone = "(" + num1 + "" + num2 + "" + num3 + ")" + "-" + set2 + "-" + set3;
        
        return cellphone;
    }
    public static String randomDate(){
        Date date = new Date();
        String var_Date = date.toString();
        
        return var_Date;
    }
   public static String[][] readCSV()
    {
        String[][] lineDetails = new String[3][2];
        
        String fileName = "C:/Users/sibusiso.khumalo/Desktop/Data.csv";
        
        String line = "";
        int j = 0;
        
        try{
            Scanner scanner_csv = new Scanner(new BufferedReader(new FileReader(fileName)));
            while(scanner_csv.hasNextLine()){
                line = scanner_csv.nextLine();
                String[] values = line.split(",");

                for(int i = 0; i < values.length; i++){
                    lineDetails[j][i] = values[i];
                }
                j++;
            }
        }
        catch(FileNotFoundException e){
            System.out.println(e);
        }
        return lineDetails;
    }
    
    //LogIn
    public static String username(){
        return "//input[@id='user_name']";
    }
    public static String password(){
        return "//input[@id='user_password']";
    }
    public static String loginButton(){
        return "//input[@id='login_button']";
    }
    
    //Create
    public static String salesButton(){
        return "//a[@id='grouptab_0']";
    }
    public static String accountsButton(){
        return "//a[@id='moduleTab_0_Accounts']";
    }
    public static String contactsButton(){
        return "//a[@id='moduleTab_0_Contacts']";
    }
    public static String createButton(){
        return "//a[@id='create_link']";
    }
    public static String txtName(int n){
        if (n==0){return "//input[@id='first_name']";} else {return "//input[@id='last_name']";}
    }
    public static String txtFirstName(){
        return "//input[@id='first_name']";
    }
    public static String txtLastName(){
        return "//input[@id='last_name']";
    }
    public static String txtOfficePhone(){
        return "//input[@id='phone_work']";
    }
    public static String txtName(){
        return "//input[@id='name']";
    }
    public static String txtCellPhone(){
        return "//input[@id='phone_mobile']";
    }
    public static String txtEmail(){
        return "//input[@id='Contacts0emailAddress0']";
    }
    
    //Validation 
    public static String vFirstName(String text){
        return "//a[contains(text(), '" + text + "')]";
    }
    public static String vFullName(){
        return "//span[@id='full_name']";
    }
    public static String vCellPhone(String text){
        return "//td[contains(text(),'" + text + "')]";
    }
    public static String extractTable(){
        return "//div[@id='detailpanel_1']//td";
    }  
    
    
    //Save And Delete
    public static String saveButton(){
        return "//input[@id='SAVE_HEADER']";
    }
    public static String selectButton(){
        return "//ul[@id='detail_header_action_menu']/..//span";
    }
    public static String deleteButton(){
        return "//a[@id='delete_button']";
    }
    public static String deleteContact(){
        return "//a[@id='delete_listview_top']";
    }
    public static String fullnameButton(String fname){
        return "//a[contains(text(),'" + fname + "')]";
    }
    public static String cDropDownButton(){
        return "//div[@id='list_subpanel_contacts']//span[@class='ab']";
    }
    public static String selectDropDownButton(){
        return "//a[@id='accounts_contacts_select_button']";
    }
    public static String selectCheckBox(String fname){
        return "//a[contains(text(),'"+ fname + "')]/../../..//input";
    }
    //Logging Out
    public static String logOutButton(){
        return "//a[@id='logout_link']";
    }
        
    
            
//etc.

    public static String HomePage() {
        return "//div[contains(text(),'Welcome')]";
    }
}
