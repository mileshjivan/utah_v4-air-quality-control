/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.IsometricsPOCPageObjects;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Sign In To Isometrix",
        createNewBrowserInstance = true
)
public class IsometricsSignIn extends BaseClass {

    String error = "";

    public IsometricsSignIn() {

    }

    public TestResult executeTest() {

        if (!NavigateToIsometricsSignInPage()) {
            return narrator.testFailed("Failed to navigate to Isometrix Sign In Page");
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToIsomoterics()) {
            return narrator.testFailed("Failed to sign into the Isometrix Home Page");
        }

        if (!IsomotericsCheckSolutionBranch()) {
            return narrator.testFailed("Failed to check IsoMetriX Solution Branch.");
        }

        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page");
    }

    public boolean NavigateToIsometricsSignInPage() {

        if (!SeleniumDriverInstance.navigateTo(IsometricsPOCPageObjects.IsometrixURL())) {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        return true;
    }

    public boolean SignInToIsomoterics() {

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsPOCPageObjects.Username(), testData.getData("Username"))) {
            error = "Failed to enter text into email text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("Username") + " to the Username Text Field");

        if (!SeleniumDriverInstance.enterTextByXpath(IsometricsPOCPageObjects.Password(), testData.getData("Password"))) {
            error = "Failed to enter text into email text field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("Password") + " to the Password Text Field");

        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.LoginBtn())) {
            error = "Failed to click sign in button.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.userProjectXpath(), 10)) {
            error = "Failed to wait user name"
                    + "";
            return false;
        }

        userName = SeleniumDriverInstance.retrieveTextByXpath(IsometricsPOCPageObjects.userProjectXpath());

        narrator.stepPassedWithScreenShot("Successfully Clicked The Sign In Button");

        return true;

    }

    private boolean IsomotericsCheckSolutionBranch() {
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsometricsPOCPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame ";
//        }

       if (!SeleniumDriverInstance.waitForElementByXpath(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to wait to switch to frame ";
            
        }
        
        if (!SeleniumDriverInstance.swithToFrameByName(IsometricsPOCPageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
            
        }

//        if (!SeleniumDriverInstance.waitForElementsByXpath(IsometricsPOCPageObjects.homepage_SolutionBranch_Btn())) {
//            error = "Failed to wait for Solution Branch button.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.homepage_SolutionBranch_Btn())) {
//            error = "Failed to click Solution Branch button.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementsByXpath(IsometricsPOCPageObjects.homepage_SolutionBranchCode_Btn())) {
//            error = "Failed to wait for Solution Branch button.";
//            return false;
//        }
//
//        String extractedText = SeleniumDriverInstance.retrieveTextByXpath(IsometricsPOCPageObjects.homepage_SolutionBranchCode_Btn());
//
//        narrator.stepPassedWithScreenShot("Successfully extracted Solution Branch: " + extractedText + ".");
//
//        if (!SeleniumDriverInstance.waitForElementsByXpath(IsometricsPOCPageObjects.backtohomepage_Btn())) {
//            error = "Failed to wait for Solution Branch button.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.backtohomepage_Btn())) {
//            error = "Failed to click Solution Branch button.";
//            return false;
//        }
        return true;
    }

}
