/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;


import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */

@KeywordAnnotation(
        Keyword = "Capture Air Quality Monitoring - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Air_Quality_Monitoring_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Air_Quality_Monitoring_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToAirQualityMonitoring())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToAirQualityMonitoring(){
        //Navigate to Tailings Management
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.tailingManagementTab())){
            error = "Failed to wait for 'Tailings Management' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.tailingManagementTab())){
            error = "Failed to click on 'Tailings Management' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management' tab.");
        
//        //Navigate to Tailings Management Monitoring
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.tailingManagementMonitoringTab())){
//            error = "Failed to wait for 'Tailings Management Monitoring' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.tailingManagementMonitoringTab())){
//            error = "Failed to click on 'Tailings Management Monitoring' tab.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management Monitoring' tab.");

        //Navigate to Air Quality Monitoring
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab())){
            error = "Failed to wait for 'Air Quality Monitoring' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab())){
            error = "Failed to click on 'Air Quality Monitoring' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Air Quality Monitoring' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.addButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.addButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
         //Process flow 2 
         pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.processFlow2())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.processFlow2())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitTab())){
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitTab())){
            error = "Failed to click 'Business Unit' dropdown.";
            return false;
        }
        //Expand Global Company
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.expandGlobalCompany())){
            error = "Failed to wait for 'Global Company' expansion";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.expandGlobalCompany())){
            error = "Failed to wait for 'Global Company' expansion";
            return false;
        }
        //Expand South Africa
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.expandSouthAfrica())){
            error = "Failed to wait for 'South Africa' expansion";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.expandSouthAfrica())){
            error = "Failed to wait for 'South Africa' expansion";
            return false;
        }
        //Victory Site
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitSelect(getData("Business Unit")))){
            error = "Failed to wait for Business Unit option: '" + getData("Business Unit") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitSelect(getData("Business Unit")))){
            error = "Failed to select Business Unit option: '" + getData("Business Unit") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Business unit : '" + getData("Business Unit") + "'.");
        
        //Air quality type
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityType())){
            error = "Failed to wait for 'Air Quality Type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityType())){
            error = "Failed to click on 'Air Quality Type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityTypeSelect(getData("Air Quality Type")))){
            error = "Failed to wait for Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityTypeSelect(getData("Air Quality Type")))){
            error = "Failed to select Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        String airQualityType = getData("Air Quality Type");
        switch(airQualityType){
            case "Emission":
                //Monitoring poin
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");
                
                if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monitoringPointTab())){
                    error = "Failed to wait for 'Monitoring point' tab.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monitoringPointTab())){
                    error = "Failed to click on 'Monitoring point' tab.";
                    return false;
                }
                
                if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monitoringPointOption(getData("Monitoring Point")))){
                    error = "Failed to wait for '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monitoringPointOption(getData("Monitoring Point")))){
                    error = "Failed to click on '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Monitoring Point : '" + getData("Monitoring Point") + "'.");
                break;
            default:
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");
        } 
        
        //Month / Year
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monthTab())){
            error = "Failed to wait for 'Month' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monthTab())){
            error = "Failed to click on 'Month' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monthOption(getData("Month")))){
            error = "Failed to wait for '" + getData("Month") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monthOption(getData("Month")))){
            error = "Failed to click on '" + getData("Month") + "' option.";
            return false;
        }
        
        //Year
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.yearTab())){
            error = "Failed to wait for 'Year' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.yearTab())){
            error = "Failed to click on 'Year' tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.yearOption(getData("Year")))){
            error = "Failed to wait for '" + getData("Year") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.yearOption(getData("Year")))){
            error = "Failed to click on '" + getData("Year") + "' tab.";
            return false;
        }
      
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveToContinueButton())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveToContinueButton())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }

//        //Saving mask
//        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
        
        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(Air_Quality_Monitoring_PageObjects.saveWaitNew())) {
            error = "Failed to wait for saving...";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully saved the record");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitorRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        narrator.stepPassedWithScreenShot("Parameter Readings and Air Quality Monitoring Findings panels are displayed");
        String retrievePanel1text = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.parameterReadingsPanel()); 
        if (!retrievePanel1text.equalsIgnoreCase("Parameter Readings")) {
                error = "Failed to retrieve Parameter Readings panel text ";
            }
        
        String retrievePanel2text = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.AQMFindingsPanel()); 
        if (!retrievePanel2text.equalsIgnoreCase("Air Quality Monitoring Findings")) {
                error = "Failed to retrieve Air Quality Monitoring Findings panel text ";
                return false;
            }

        return true;
    }

}
