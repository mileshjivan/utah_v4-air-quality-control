/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;


import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Findings",
        createNewBrowserInstance = false
)

public class FR4_Capture_Findings_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR4_Capture_Findings_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToCaptureFindings())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Findings");
    }

    public boolean navigateToCaptureFindings(){
        //Navigate to Capture Findings
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_panel())){
            error = "Failed to wait for 'Capture Findings' panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_panel())){
            error = "Failed to click on 'Capture Findings' panel.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Capture Findings' tab.");
        
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked Add button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
         //Process flow 
          pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_processflow(), 10000)){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_processflow())){
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        //Finding description
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_desc())){
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.AQMF_desc(), testData.getData("Description"))){
            error = "Failed to enter text '"+testData.getData("Description")+"' into 'Description' textarea.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Entered text into description.");
        
        //Finding owner
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_owner_dropdown())){
            error = "Failed to wait for 'Finding owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_owner_dropdown())){
            error = "Failed to click on 'Finding owner' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_owner_select(testData.getData("Owner")))){
            error = "Failed to wait for Finding owner option: '" + testData.getData("Owner") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_owner_select(testData.getData("Owner")))){
            error = "Failed to select Finding owner option: '" + testData.getData("Owner") + "'.";
            return false;
        }
        
        //Risk source
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_risksource_dropdown())){
            error = "Failed to wait for 'Risk source' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_risksource_dropdown())){
            error = "Failed to click on 'Risk source' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_risksource_select(testData.getData("Risk Option1")))){
            error = "Failed to wait for Risk source option: '" + testData.getData("Risk Option1") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_risksource_select(testData.getData("Risk Option1")))){
            error = "Failed to select Risk source option: '" + testData.getData("Risk Option1") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_risksource_select2(testData.getData("Risk Option1"), testData.getData("Risk Option2")))){
            error = "Failed to wait for Risk source option: '" + testData.getData("Risk Option2") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_risksource_select2(testData.getData("Risk Option1"), testData.getData("Risk Option2")))){
            error = "Failed to select Risk source option: '" + testData.getData("Risk Option2") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_arrow())){
            error = "Failed to wait for 'dropdown arrow' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_arrow())){
            error = "Failed to click on 'dropdown arrow' dropdown.";
            return false;
        }
        //Finding classification
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_class_dropdown())){
            error = "Failed to wait for 'Finding classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_class_dropdown())){
            error = "Failed to click on 'Finding classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_class_select(testData.getData("Classification")))){
            error = "Failed to wait for Finding classification option: '" + testData.getData("Classification") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_class_select(testData.getData("Classification")))){
            error = "Failed to select Finding classification option: '" + testData.getData("Classification") + "'.";
            return false;
        }
        
        //Risk
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_risk_dropdown())){
            error = "Failed to wait for 'Risk' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_risk_dropdown())){
            error = "Failed to click on 'Risk' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_risk_select(testData.getData("Risk")))){
            error = "Failed to wait for Risk option: '" + testData.getData("Risk") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_risk_select(testData.getData("Risk")))){
            error = "Failed to select Risk option: '" + testData.getData("Risk") + "'.";
            return false;
        }
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }

//        //Saving mask
//        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(Air_Quality_Monitoring_PageObjects.saveWaitNew())) {
            error = "Failed to wait for saving...";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moves to Edit phase");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.findingsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    

        return true;
    }

}
