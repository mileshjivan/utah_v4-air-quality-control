/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;


import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Dust Measurements MainScenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Dust_Measurements_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Capture_Dust_Measurements_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToMeasurements())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToMeasurements(){
        //Navigate to Measurement Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementTab())){
            error = "Failed to wait for 'Measurements' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurement())){
            error = "Failed to click on 'Measurements' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Measurements' panel.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementsAddButton())){
            error = "Failed to wait for 'Measurements' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementsAddButton())){
            error = "Failed to click on 'Measurements' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Measurements Add' button.");
        
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Measurements Process flow  
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementProcessFlow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementProcessFlow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
        //Monitoring point
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringTab())){
            error = "Failed to wait for 'Monitoring point' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringTab())){
            error = "Failed to click on 'Monitoring point' tab.";
            return false;
        }

        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringOption(getData("Monitoring Point1")))){
            error = "Failed to wait for '" + getData("Monitoring Point1") + "' option.";

        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringOption(getData("Monitoring Point1")))){
            error = "Failed to wait for '" + getData("Monitoring Point") + "' option.";

            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringOption(getData("Monitoring Point1")))){
            error = "Failed to click on '" + getData("Monitoring Point1") + "' option.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringOption2(getData("Monitoring Point2")))){
            error = "Failed to wait for '" + getData("Monitoring Point2") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringOption2(getData("Monitoring Point2")))){
            error = "Failed to click on '" + getData("Monitoring Point2") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Monitoring Point2 : '" + getData("Monitoring Point2") + "'.");
        
        //Measurement
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementFieldTab())){
            error = "Failed to wait for 'Measurement' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.measurementField(), getData("MeasurementNo"))){
            error = "Failed to enter '" + getData("MeasurementNo") + "' into field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Measurement : '#" + getData("MeasurementNo") + "'.");
        
        //Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.unitTab())){
            error = "Failed to wait for 'Unit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.unitTab())){
            error = "Failed to click on 'Unit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.unitOption(getData("Unit")))){
            error = "Failed to wait for '" + getData("Unit") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.unitOption(getData("Unit")))){
            error = "Failed to click on '" + getData("Unit") + "' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Unit: '" + getData("Unit") + "'.");
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementSaveButton())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementSaveButton())){
            error = "Failed to click on 'Save' button.";
            return false;
        }

//        //Saving mask
//        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(Air_Quality_Monitoring_PageObjects.saveWaitNew())) {
            error = "Failed to wait for saving...";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moves to Edit phase");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.dustMeasureRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    

        
        return true;
    }

}
