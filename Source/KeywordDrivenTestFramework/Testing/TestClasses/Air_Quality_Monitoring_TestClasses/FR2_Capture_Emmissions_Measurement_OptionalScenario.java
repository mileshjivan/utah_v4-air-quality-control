/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;


import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Emissions Measurement - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Emmissions_Measurement_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Emmissions_Measurement_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        if(!uploadDocument()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

   
    public boolean uploadDocument(){
        //Add support document
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_uploadLinkbox())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_uploadLinkbox())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.LinkURL(), getData("Document Link") )){
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.urlTitle(), getData("Title"))){
            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");
        
         //switch to the iframe

//        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.iframeName())) {
            error = "Failed to locate to switch to frame.";
        }
        
         if (!SeleniumDriverInstance.swithToFrameByName(Air_Quality_Monitoring_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
        }

        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }

//        //Saving mask
//        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(Air_Quality_Monitoring_PageObjects.saveWaitNew())) {
            error = "Failed to wait for saving...";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully uploaded documents and processflow remains in Edit phase");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.emissionMeasureRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    

        return true;
    }

}
