/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;


import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;


import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Emissions Measurement - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Emmissions_Measurement_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Emmissions_Measurement_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToParameterRedings())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToParameterRedings(){
        //Navigate to Parameter Readings
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameter_panel())){
            error = "Failed to wait for 'Parameter Readings' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameter_panel())){
            error = "Failed to click on 'Parameter Readings' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Parameter Readings' tab.");
        
        //Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
         //CEM Process flow 
          pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_processFlow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_processFlow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        
        //Parameter
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameters_dropdown())){
            error = "Failed to wait for 'Parameter' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameters_dropdown())){
            error = "Failed to click on 'Parameter' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameters_select(getData("Parameter")))){
            error = "Failed to wait for Parameter option: '" + getData("Parameter") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameters_select(getData("Parameter")))){
            error = "Failed to select Parameter option: '" + getData("Parameter") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Parameter : '" + getData("Parameter") + "'.");
        
        //Measurement
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Measurement())){
            error = "Failed to wait for 'Measurement' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.CEM_Measurement(), getData("Measurement"))){
            error = "Failed to enter '" + getData("Measurement") + "' into Measurement field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Measurement : '" + getData("Measurement") + "'.");
        
        //Sample Date
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Sdate())){
            error = "Failed to wait for 'Sample date' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.CEM_Sdate(), getData("Sample Date"))){
            error = "Failed to enter '" + getData("Sample Date") + "' into 'Sample date' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Sample Date : '" + getData("Sample Date") + "'.");
        
        //Sample taken
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_stakenby())){
            error = "Failed to wait for 'Taken by' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.CEM_stakenby(), getData("Taken by"))){
            error = "Failed to enter '" + getData("Taken by") + "' into 'taken by' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Taken by : '" + getData("Taken by") + "'.");
        
        //Comments
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_comments())){
            error = "Failed to wait for 'Comments' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.CEM_comments(), getData("Comments"))){
            error = "Failed to enter '" + getData("Comments") + "' into 'Comments' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Comments : '" + getData("Comments") + "'.");
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Save())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Save())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }

//        //Saving mask
//        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
        
        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(Air_Quality_Monitoring_PageObjects.saveWaitNew())) {
            error = "Failed to wait for saving...";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moves to Edit phase");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.emissionMeasureRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    

        return true;
    }

}
