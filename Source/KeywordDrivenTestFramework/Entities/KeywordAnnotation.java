/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Entities;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;




/**
 *
 * @author fnell
 */


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface KeywordAnnotation 
{
    String Keyword();
    //Use to restart - init the selenium instance
    boolean createNewBrowserInstance() default false;
    
    //Use to init the Appium Instance - currently wont restart
    boolean appiumTest() default false;
    
    //Use to init WinAppDriver
    boolean winAppTest() default false;
    
    //Use to mark a test as skippable
    //This will mean the test will log a skip if the previous test failed
    boolean blockable() default false;
}
